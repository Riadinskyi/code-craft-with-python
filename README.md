# Code Craft with Python

Курс розробки програмного забезпечення за допомогою мови програмування Python для студентів другого курсу ХНУ ім. В. Н. Каразіна

## Getting started
[План Курсу](course-overview.md)

#### Конспекти
- [Заняття 1](notes/class-1.md)


## Authors and acknowledgment
Author: Mykhailo Dalchenko. The course is heavily based on [Allen B. Downey "Think Python: How to Think Like a Computer Scientist"](https://greenteapress.com/wp/think-python-2e/).

## License
[MIT License](LICENSE.md)
